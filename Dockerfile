#копируем образ докера
FROM node 

#выполняем команду создаем папку приложения внутри контейнера
RUN mkdir /app

#копируем файлы
COPY . /app

#указываем рабочую директорию
WORKDIR /app

#выполняем команды
RUN yarn install
RUN yarn test
Run yarn build
CMD yarn start

#указываем порт на котором работает приложение
EXPOSE 3000


